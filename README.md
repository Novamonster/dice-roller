Dice Roller Documentation  
Created by: Elio Martos

Overview  
The Dice Roller project is a web-based application that allows users to roll dice and keep track of roll history. It provides a simple user interface to input the number of rolls and sides on a die, and it generates random numbers based on the input. The application also maintains a roll history, which allows users to view the frequency of each roll for different dice configurations.  

Installation Guide  
Install Git on the new computer if it's not already installed. You can download the official Git distribution for your operating system from https://git-scm.com/. To install and run the Dice Roller on your local machine, follow these steps:

1.	Ensure that you have Python3 installed on your system.
2.	Clone the project repository from GitHub:
    **git clone https://github.com/Novamonster/dice-roller.git**
3.	Navigate to the project directory:
    **cd dice-roller**
4.	Create a virtual environment.(optional) 
5.	Install the project dependencies:
    **pip install -r requirements.txt** 
6.	Start the API
    **python roller_api.py**
7.	Start the application:
    **app.html**
 
Testing Instructions
1.	Open the command prompt in the base directory.
2.	Then using pytest run the tests.
***The probability_test takes around 4 minutes to complete, if you don’t care to test that you can run the tests separately.***

	
Usage Instructions
1.	Access the Dice Roller application in your web browser.
2.	Enter the desired number of rolls and sides on the dice in the input fields provided.
3.	Click the "Roll" button to generate random numbers based on your input.
4.	The result of the dice roll will be displayed, and the roll history will be updated.
5.	The roll history will be displayed, showing the frequency of each roll for different dice configurations.
 
Additional Notes

1.	It is recommended to ensure that the necessary ports (e.g., port 8585) are not blocked by firewalls. 

2.	Ensure that the SQLite database file (roll_history.db) is present and writable by the application.

3.	If you need to reset the history, just delete the roll_history.db file in your directory.

