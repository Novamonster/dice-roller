#Path: roller_api.py
#usr/bin/env python3

'''
This is the class that rolls the dice.
'''
import datetime
import time
class NumberGenerator(object):
    '''Class that does the dice rlling'''   
    def __init__(self, sides_on_die = 6, number_of_rolls = 1, *args, **kwargs):
        """Takes in 2 integers and generates a random integer

        Parameters
        ----------
        sides_on_die : int, optional
            An integer that defaults to 6. Human input chosen
            at the beginning to represent the number of side on a die. 
            Determines the range at which the random number will be in.
        number_of_rolls : int, optional
            An integer that defaults to 1. Human input
            chosen at the beginning to represent the number of random numbers
            generated.

        Examples A: Basic Usage
        --------
        >>> from dice_roller import NumberGenerator
        >>> ng = NumberGenerator()
        >>> ng.generateRandomNumbers(6, 1)
        [4]

        Examples B: Multi Usage
        --------
        >>> from dice_roller import NumberGenerator
        >>> ng = NumberGenerator()
        >>> ng.generateRandomNumbers(6, 2)
        [3, 6]
        
        """

        self.sides_on_die = sides_on_die
        self.number_of_rolls = number_of_rolls
        self.generateRandomNumbers(self.sides_on_die, self.number_of_rolls)
        
    def middleSquare(self, seed: int, sides_on_die: int) -> int:
            '''
            Takes the seed and using the middle square number method it creates a random number.
            It squares the seed then if the seed length is not an even number it fills it the
            beggining with zeros and based on the sides_on_die argument it will return that many
            integers frim the middle of the squared seed.
            '''   

            seed **= 2

            if seed % 2 == 0:
                seed = str(seed).zfill(len(str(seed))+1)
            else:
                seed = str(seed)

            if sides_on_die > 9 and sides_on_die <= 100:
                self.first_number = int(seed[len(seed)//2])           
                self.second_number = int(seed[len(seed)//2-1])
                self.middle = int(str(self.first_number) + str(self.second_number))
                if self.middle == 0:
                    self.middle = 100

            if sides_on_die < 10:
                if sides_on_die == 1:
                    self.middle = 1
                    return self.middle
                self.middle = int(seed[len(seed)//2])
            return self.middle 

    def generateRandomNumbers(self, sides_on_die: int, number_of_rolls: int) -> list:
            '''
            Creates a seed based on the time it was called then it buffers to make sure
            the next seed will be different. 
            Calls self.middleSquare function based on the number_of_rolls argument until
            the integer generated is in the range of the sides_on_die argument and its not 0.
            Then it appends it to a list and returns it
            '''

            random_number = []

            for number in range(1, number_of_rolls + 1):
                seed = int(str(datetime.datetime.now())[-6:])
                time.sleep(.0001)
                self.middleSquare(seed, sides_on_die)
                while self.middle not in range(1, sides_on_die+1) or self.middle == 0:
                    seed = int(str(datetime.datetime.now())[-6:])
                    time.sleep(.0001)   
                    self.middleSquare(seed, sides_on_die)
                random_number.append(self.middle)
            return random_number
    