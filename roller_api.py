#Path: roller_api.py
#usr/bin/env python3

"""
This is the API for the dice roller application.

Has functionality to do:
    Initialization of the database (initDb)
    Run the flask application on port 8585 (app.run)
"""

from flask import Flask  #A web application framework written in Python
from flask import jsonify #Turns python objects into JSON
from flask import request #HTTP request
from flask import g #Used to store global variables within the context
from flask_cors import CORS #Enables CORS support on all routes, for all origins and methods.
from dice_roller import NumberGenerator #Class to create random numbers
import sqlite3 #Provides an SQL interface

DATABASE = 'roll_history.db'
app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

def getDb() -> sqlite3.Connection:
    """
    Establishes a connection to the database
    Tries to assign the _database attribute to db
    If it fails, the creates the atrribute and then assigns it,
    then it changes the format of the SQLite database rows to dictionaries

    Returns:
    --------
    sqlite3.Connection: 
        Connection to the global application database.    
    """    
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db


def initDb(app: object) -> None:
    """
    Within the context it obtains the connection,
    Creates a way to interact with the database using cursor
    Then it creates a table if it doesn't already exist with the necessary columns and rows

    Parameters
    ----------
    app : object
        Flask application object to initiate the database.
    """    
    with app.app_context():
        db = getDb()
        cursor = db.cursor()
        cursor.execute(
        '''
        CREATE TABLE IF NOT EXISTS rolls (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            sides INTEGER, 
            roll INTEGER, 
            count INTEGER,
            UNIQUE(sides, roll)
        );
        '''
    )
        cursor.close()
        db.commit()


@app.route('/roll', methods=['POST'])
def roll():
    """
    Generates random dice rolls using the NumberGenerator class.
    Makes the count of that side of the dice and roll combination 1,
    if it already exists then it instead increases it by 1.

    Returns:
    --------
    JSON object:
        Returns a response converted into JSON containing the generated rolls.
    """
    data = request.json
    sides_on_die = data.get('sides_on_die')
    number_of_rolls = data.get('number_of_rolls')
    #Generates random numbers
    generator = NumberGenerator(sides_on_die, number_of_rolls)
    rolls = generator.generateRandomNumbers(sides_on_die, number_of_rolls)

    db = getDb()
    cursor = db.cursor()
    #Updates the roll history for the current die
    for roll in rolls:
        cursor.execute(
            'INSERT INTO rolls (sides, roll, count) VALUES (?, ?, 1) ON CONFLICT (sides, roll) DO UPDATE SET count = count + 1',
            (sides_on_die, roll)
            )
    cursor.close()
    db.commit()

    return jsonify({'rolls': rolls})


@app.route('/roll-history', methods=['GET'])
def getRollHistory():
    """
    Within the context it obtains the connection,
    Creates a way to interact with the database using cursor
    It fetches the rolls from the table
    The rolls are transformed into a dictionary where the keys represent the number of sides,
    and the values of the sides key are dictionaries containing roll values and their counts for each side.

    Returns:
    --------
    JSON object:
        Returns a dictionary converted into JSON.
    """
    db = getDb()
    cursor = db.cursor()
    cursor.execute('SELECT sides, roll, count FROM rolls')
    rolls = cursor.fetchall()
    cursor.close()
    roll_history = {}
    for row in rolls:
        sides = row['sides']
        roll = row['roll']
        count = row['count']
        if sides not in roll_history:
            roll_history[sides] = {}
        roll_history[sides][roll] = count
    return jsonify(roll_history)

if __name__ == '__main__':
    initDb(app)
    app.run(host="localhost", port=8585)
