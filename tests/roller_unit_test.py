import pytest
from dice_roller import NumberGenerator


def test_generateRandomNumbers():
    # Test case 1: sides_on_die = 6, number_of_rolls = 1
    ng = NumberGenerator(sides_on_die = 6, number_of_rolls = 1)
    random_numbers = ng.generateRandomNumbers(6, 1)
    assert len(random_numbers) == 1, "Expecting 1 random number"
    assert all(1 <= num <= 6 for num in random_numbers),  "Expecting numbers between 1 and 6"

    # Test case 2: sides_on_die = 10, number_of_rolls = 3
    ng = NumberGenerator(sides_on_die = 10, number_of_rolls = 3)
    random_numbers = ng.generateRandomNumbers(10, 3)
    assert len(random_numbers) ==  3, "Expecting 3 random numbers"
    assert all(1 <= num <= 10 for num in random_numbers),  "Expecting numbers between 1 and 10"

def test_middleSquare():
    # Test case 1: seed = 123456, sides_on_die = 6
    ng = NumberGenerator()
    result = ng.middleSquare(123456, 6)
    assert 1 <= result,  "Expecting result between 1 and 6"
    assert result <= 6,  "Expecting result between 1 and 6"

    # Test case 2: seed = 987654, sides_on_die = 10
    ng = NumberGenerator()
    result = ng.middleSquare(987654, 10)
    assert 1 <= result,  "Expecting result between 1 and 10"
    assert result <= 10,  "Expecting result between 1 and 10"
