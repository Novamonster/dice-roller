import pytest
from dice_roller import NumberGenerator

@pytest.fixture
def number_generator():
    return NumberGenerator()

def test_generator(number_generator):
    number_generator.generateRandomNumbers(6, 1)
