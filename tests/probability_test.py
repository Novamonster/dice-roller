import pytest
from collections import Counter
from dice_roller import NumberGenerator

@pytest.fixture
def number_generator():
    return NumberGenerator()

def test_generator(number_generator):
    result = number_generator.generateRandomNumbers(6, 10000)
    counter = Counter(result)
    calculated_values = {k:v for k, v in counter.items()}
    print(calculated_values)