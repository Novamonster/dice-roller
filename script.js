//Variable declarations
const rollButton = document.getElementById("rollButton");
const dropdown = document.getElementById("dropdown");
const numberInput = document.getElementById("numberInput");
const sidebar = document.getElementById("sidebar");
const rollResult = document.getElementById("rollResult");
const errorLabel = document.getElementById("errorLabel")

//Makes a GET request to the URL and if it is fulfilled then it converts it into JSON.
//Run code only after page has loaded
window.onload = () => {
fetch("http://localhost:8585/roll-history")
  .then((response) => response.json())
  .then((data) => {
    //The rollHistory variable is set to the JSON value.
    const rollHistory = data;
    //Checks if rollHistory exists
    if (!rollHistory) {
      rollHistory = {};
    }
    //Dictates what will happne when the roll button is pressed
    rollButton.addEventListener("click", () => {
      //Variables are set to the values from the roll number and number of sides.
      const numberOfRolls = parseInt(dropdown.value);
      const numberOfSides = parseInt(numberInput.value);

      //Checks if there is invalid input was entered for those values, if so the error label is displayed.
      if (isNaN(numberOfSides) || numberOfSides < 1 || numberOfSides > 100) {
        resultBox.style.display = "block";
        rollResult.value = "";
        errorLabel.textContent = "Please enter a number between 1 and 100";
        errorLabel.style.visibility = "visible"; // Show the error label
        return;
      }

      //If a valid input is given, hide the error label
      errorLabel.style.visibility = "hidden";

      //Makes a POST request to the URL with a header and a body
      fetch("http://localhost:8585/roll", {
        method: "POST",
        //The payload is going to be in JSON format
        headers: {
          "Content-Type": "application/json",
        },
        /*Body: Converts the JS onject into JSON string
        They correspond to the number of sides on the die and the number of rolls specified by the user, respectively*/
        body: JSON.stringify({
          sides_on_die: numberOfSides,
          number_of_rolls: numberOfRolls,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
        //Checks within the .then() block to make sure the response by the server is correct
          if (data.error) {
            console.error(data.error);
            return;
          }
          //Assigns results the array of numbers produced
          const results = data.rolls;

          //Checks if the row of that number of sides exists
          if (!rollHistory[numberOfSides]) {
            rollHistory[numberOfSides] = {};
          }
          /*For every number that was rolled on, it checks if the value is already
           there if not it makes it one if it is it increases it by 1.*/
          for (const result of results) {
            if (!rollHistory[numberOfSides][result]) {
              rollHistory[numberOfSides][result] = 1;
            } else {
              rollHistory[numberOfSides][result]++;
            }
          }

          //It separates each result with a comma
          rollResult.value = results.join(", ");
          //Makes the resultBox appear in the page
          resultBox.style.display = "block";

          //Updates the sidebar with the roll history for the current die
          updateSidebar(numberOfSides);
        })
        //If there is an error it displays it in the console
        .catch((error) => {
          console.error(error);
        });
    });

    //Function to update the sidebar with the roll history
    const updateSidebar = (numberOfSides) => {
      //Clears sidebar
      sidebar.innerHTML = "";
      //Saguard if numberOfSides doesn't exist
      if (!numberOfSides) 
        return;

      const rolls = rollHistory[numberOfSides];
      //Saguard if rolls doesn't exist
      if (!rolls) 
        return; 
      //Creates the HTML for the sidebar
      const sidebarItem = document.createElement("div");
      sidebarItem.className = "sidebar-item";
      const title = document.createElement("h2");
      title.textContent = `A die with ${numberOfSides} sides`;
      sidebarItem.appendChild(title);
      //For each roll it shows the number and how many times it has been rolled
      for (const roll in rolls) {
        const count = rolls[roll];
        const listItem = document.createElement("p");
        listItem.textContent = `${roll} has been rolled: ${count}`;
        sidebarItem.appendChild(listItem);
      }
      //Makes sure the info if inside of the sidebar element
      sidebar.appendChild(sidebarItem);
    };

  })
  //If there is an error it displays it in the console
  .catch((error) => {
    console.error(error);
  })
};
